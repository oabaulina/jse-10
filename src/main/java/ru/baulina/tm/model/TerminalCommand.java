package ru.baulina.tm.model;

public class TerminalCommand {

    private String name = "";

    private String arg = "";

    private String discription = "";

    public TerminalCommand(String name, String arg, String discription) {
        this.name = name;
        this.arg = arg;
        this.discription = discription;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public TerminalCommand() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (discription != null && !discription.isEmpty()) result.append(": ").append(discription);

        return result.toString();
    }

}
