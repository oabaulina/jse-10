package ru.baulina.tm.bootstrap;

import ru.baulina.tm.api.ICommandController;
import ru.baulina.tm.api.ICommandRepository;
import ru.baulina.tm.api.ICommandService;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.controller.CommandController;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final  String[] args) {
        commandController.displayWelcome();
        if (parseCommands(args) || parseArgs(args)) commandController.exit();
        process();
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CommandConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return false;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                return true;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                return true;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                return true;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                return true;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                return true;
            case ArgumentConst.INFO:
                commandController. showInfo();
                return true;
            default:
        }
        return false;
    }

    private boolean parseCommands(final String[] commands) {
        if (commands == null || commands.length == 0) return false;
        final String command = commands[0];
        return parseCommand(command);
    }

    private boolean parseCommand(final String command) {
        if (command == null || command.isEmpty()) return false;
        switch (command.toLowerCase()) {
            case CommandConst.HELP:
                commandController.showHelp();
                return true;
            case CommandConst.ABOUT:
                commandController.showAbout();
                return true;
            case CommandConst.VERSION:
                commandController.showVersion();
                return true;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                return true;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                return true;
            case CommandConst.INFO:
                commandController.showInfo();
                return true;
            case CommandConst.EXIT:
                commandController.exit();
                return true;
            default:
        }
        return false;
    }

}
