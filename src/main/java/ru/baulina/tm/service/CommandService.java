package ru.baulina.tm.service;

import ru.baulina.tm.api.ICommandRepository;
import ru.baulina.tm.api.ICommandService;
import ru.baulina.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
