package ru.baulina.tm.constant;

public class CommandDescriptionConst {

    public static final String HELP = "Display list of terminal commands.";

    public static final String VERSION = "Display program version.";

    public static final String ABOUT = "Display developer info.";

    public static final String EXIT = "Close application.";

    public static final String INFO = "Display information about system.";

    public static final String ARGUMENTS = "Show program arguments.";

    public static final String COMMANDS = "Show program commands.";

}
