package ru.baulina.tm.api;

import ru.baulina.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
